<?php

namespace myPhpAdminer;

/**
 * Class Adminer
 *
 * @package myPhpAdminer
 */
class Adminer
{
    public function run()
    {
        include( dirname( __DIR__ ) . '/adminer-4.7.7-en.php' );
    }
}
