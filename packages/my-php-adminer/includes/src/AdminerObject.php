<?php

namespace myPhpAdminer;

/**
 * Class AdminerObject
 *
 * @package myPhpAdminer
 */
class AdminerObject extends \Adminer
{
    public function name()
    {
        return 'My Adminer!';
    }
}
